import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { Http, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import { ListaPage } from '../lista/lista';

@IonicPage()
@Component({
  selector: 'page-cadastro',
  templateUrl: 'cadastro.html',
})
export class CadastroPage {

	private dados;
	private http: Http;
	private alertCtrl: AlertController;
	private url: string = "http://localhost/projeto_webservice/api/?acao=";	

	constructor(public navCtrl: NavController, public navParams: NavParams, http: Http, alertCtrl: AlertController) {

	    this.dados = {};
        this.dados.nome = '';
        this.dados.descricao = '';
        this.dados.valor = 0;

        this.http = http;
        this.alertCtrl = alertCtrl; 

	}

	//Cadastrar Produto
	formSubmit() {

		//Formato JSON this.dados
		//   let item = {
		//     nome: 'Item1', 
		//     descricao: 'Teste Item1', 
		//     valor: 10
		//   };

		var headers = new Headers();
		headers.append("Accept", 'application/json');
		headers.append('Content-Type', 'application/json' );
		let options = new RequestOptions({ headers: headers });

		var result = this.http.post(this.url + 'cadastrarProduto', this.dados, options)
		.map(res => res.json())
		.subscribe(data => {
		  console.log(data);
		}, error => {
		  console.log(error);
		});

		console.log(this.dados);

		let alert = this.alertCtrl.create({
			title: 'Produto cadastrado com sucesso!',
			buttons: [
				{
			        text: 'OK',
			        role: 'ok',
			        handler: () => {
			          this.navCtrl.push(ListaPage)
			        }
		      	}
		    ]
			});
		alert.present();

	}

}

import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, Platform, ViewController, AlertController } from 'ionic-angular';
import { Http, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import { ModalContentPage } from '../lista/modal';
import { ModalContentPageEditar } from '../lista/modal-editar';

import { GlobalServicesVarsProvider } from '../../providers/global-services-vars/global-services-vars';

@IonicPage()
@Component({
  selector: 'page-lista',
  templateUrl: 'lista.html',
})
export class ListaPage {

  public feeds: Array<string>;
  private url: string;
  private alertCtrl: AlertController;

  constructor(public navCtrl: NavController, public http: Http, public modalCtrl: ModalController, alertCtrl: AlertController, public globalSvsVars: GlobalServicesVarsProvider) {

    this.alertCtrl = alertCtrl;
    this.url = globalSvsVars.apiUrl;

    //Listar Produtos
    this.http.get(this.url + 'listarProdutos').map(res => res.json())
      .subscribe(data => {
        this.feeds = data.Produtos;       
      });
  }

  itemDetalhes(event, item) {
    let modal = this.modalCtrl.create(ModalContentPage, item);
    modal.present();
  }

  editarItem(event, item){

    let index = this.feeds.indexOf(item);
    let modal = this.modalCtrl.create(ModalContentPageEditar, item);
    modal.present();
    modal.onDidDismiss(dados=>{
      if(index > -1){
        this.feeds[index] = dados;
      }  
      //console.log("Data =>", item)
    });

  }

  deletarItem(event, item){
 
    let prompt = this.alertCtrl.create({
      title: 'Deseja excluir este item?',
      inputs: [{
          name: 'title'
      }],
      buttons: [
          {
              text: 'Não'
          },
          {
              text: 'Sim',
              handler: data => {
                
                let index = this.feeds.indexOf(item);
 
                if(index > -1){
                    this.feeds.splice(index, 1);
                }

                this.http.get(this.url + 'excluirProdutoPorId&idProduto=' + item.id)
                .map(res => res.json())
                .subscribe(data => {
                  console.log(data);
                }, error => {
                  console.log(error);
                });

              }
          }
      ]
    });

    prompt.present();

  }

}
